package ar.edu.unlu.dados;

import java.util.concurrent.ThreadLocalRandom;

public class Dado
{
	// N�mero m�ximo de caras del dado
	private int numeroDeCaras;
	
	// Cara actual del dado (la que est� visible y cuenta)
	private int caraActual;
	
	public Dado(int caras)
	{
		this.numeroDeCaras = caras;
	}
	
	public void tirar()
	{
		// Genero un n�mero aleatorio entre 1 y la cantidad de caras del dado
		int numeroAleatorio = ThreadLocalRandom.current().nextInt(1, this.numeroDeCaras + 1);
		
		// Asigno el n�mero generado como cara actual del dado
		this.caraActual = numeroAleatorio;
	}
	
	public int obtenerCaraActual()
	{
		// Retorno la cara actual
		return this.caraActual;
	}
	
	public int obtenerNumeroDeCaras()
	{
		// Retorno la cantidad de caras
		return this.numeroDeCaras;
	}
}
