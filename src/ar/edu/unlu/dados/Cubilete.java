package ar.edu.unlu.dados;

import java.util.ArrayList;
import java.util.Collection;

public class Cubilete
{
	// Colecci�n gen�rica de dados
	private Collection<Dado> dados;
	
	public Cubilete()
	{
		// Inicializo la colecci�n con un ArrayList
		this.dados = new ArrayList<Dado>();
	}
	
	public void agregarDado(int cantidadCaras)
	{
		// Creo un nuevo un dado
		Dado nuevoDado = new Dado(cantidadCaras);
		
		// Lo agrego a la coleccion
		dados.add(nuevoDado);
	}
	
	public String tirar()
	{
		// Datos necesarios para generar el reporte
		String resultado = "";
		int total = 0;
		
		// Recorro la coleccion de dados
		for (Dado dado : this.dados)
		{
			// Tiro el dado
			dado.tirar();
			
			// Acumulo el resultado para el total
			total = total + dado.obtenerCaraActual();
			
			// Guardo el resultado individual
			resultado = resultado + System.getProperty("line.separator") + "El dado d" + dado.obtenerNumeroDeCaras() + " sac� " + dado.obtenerCaraActual() + ".";
		}
		
		// Agrego el acumulao al reporte
		resultado = resultado + System.getProperty("line.separator") + "-----------------" + System.getProperty("line.separator") + "El resultado acumulado de la tirada fue " + total + ".";
		
		// Retorno el reporte
		return resultado;		
	}
	
	public void vaciar()
	{
		// Vac�o la colecci�n de dados
		this.dados.clear();
	}

}
