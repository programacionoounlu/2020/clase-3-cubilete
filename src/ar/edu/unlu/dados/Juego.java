package ar.edu.unlu.dados;

public class Juego
{
	public static void main(String[] args)
	{
		// Insntacio un cubilete de prueba
		Cubilete cubilete = new Cubilete();
		
		// Agrego algunos dados...
		cubilete.agregarDado(4);
		cubilete.agregarDado(6);
		cubilete.agregarDado(10);
		
		// Hago la tirada y muestro el reporte en pantalla		
		System.out.println(cubilete.tirar());
		
		// Vac�o el cubilete
		cubilete.vaciar();
		
		// Agrego otros dados...
		cubilete.agregarDado(12);
		cubilete.agregarDado(20);
		cubilete.agregarDado(100);
		
		// Hago la tirada y muestro el reporte en pantalla		
		System.out.println(cubilete.tirar());
	}
}
